package com.example.transferservice.exception;

public class FundTransferFailure extends RuntimeException {
    public FundTransferFailure(String message) {
        super(message);
    }

    public FundTransferFailure(String message, Throwable th) {
        super(message, th);
    }

}

