package com.example.transferservice.service;

import com.example.transferservice.dao.AccountDao;
import com.example.transferservice.dao.TransactionDao;
import com.example.transferservice.exception.AccountNotFoundException;
import com.example.transferservice.exception.InsufficientFundsException;
import com.example.transferservice.model.Account;
import com.example.transferservice.model.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class AccountTransferService {

    private static final Logger LOG = LoggerFactory.getLogger(AccountTransferService.class);

    @Autowired
    AccountDao accountDao;

    @Autowired
    TransactionDao transactionDao;

    public void transfer(Transaction transaction) {
        Long sourceAccountNumber = transaction.getSourceAccountNumber();
        Long destinationAccountNumber = transaction.getDestinationAccountNumber();
        BigDecimal amount = transaction.getAmount();

        // retrieve source and destination accounts with lock
        List<Account> accounts = accountDao.lockAccounts(Arrays.asList(transaction.getSourceAccountNumber(), transaction.getDestinationAccountNumber()));

        Optional<Account> sourceOptional = accounts.stream().filter(a -> a.getAccountNumber().equals(sourceAccountNumber)).findFirst();
        if (!sourceOptional.isPresent()) {
            throw new AccountNotFoundException(String.format("Account not found: %s", sourceAccountNumber));
        }

        Optional<Account> destinationOptional = accounts.stream().filter(a -> a.getAccountNumber().equals(destinationAccountNumber)).findFirst();
        if (!destinationOptional.isPresent()) {
            throw new AccountNotFoundException(String.format("Account not found: %s", sourceAccountNumber));
        }

        Account source = sourceOptional.get();
        if (source.getBalance().compareTo(amount) < 0) {
            throw new InsufficientFundsException(String.format("Account %s does not have sufficient funds", Long.toString(sourceAccountNumber, 12)));
        }

        Account destination = destinationOptional.get();

        // subtract amount from source account
        Account updatedSourceAccount = new Account.Builder()
                .withAccountNumber(source.getAccountNumber())
                .withBalance(source.getBalance().subtract(amount))
                .build();

        // add amount to destination account
        Account updatedDestinationAccount = new Account.Builder()
                .withAccountNumber(destination.getAccountNumber())
                .withBalance(destination.getBalance().add(amount))
                .build();

        // save updated accounts
        accountDao.save(updatedSourceAccount);
        accountDao.save(updatedDestinationAccount);

        // save transaction
        transactionDao.save(transaction);
    }
}
