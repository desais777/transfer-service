package com.example.transferservice.dao;

import com.example.transferservice.model.Account;

import java.util.List;

public interface AccountDao {

    void save(Account account);

    List<Account> lockAccounts(List<Long> accountNumber);
}
