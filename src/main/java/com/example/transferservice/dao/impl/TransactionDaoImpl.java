package com.example.transferservice.dao.impl;

import com.example.transferservice.dao.TransactionDao;
import com.example.transferservice.exception.FundTransferFailure;
import com.example.transferservice.model.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

/**
 * Database implementation for TransactionDao
 */
public class TransactionDaoImpl implements TransactionDao {

    private static final Logger LOG = LoggerFactory.getLogger(AccountDaoImpl.class);

    @Autowired
    NamedParameterJdbcTemplate jdbcTemplate;

    @Override
    public void save(Transaction transaction) {
        LOG.info("save is invoked with: " + transaction);

        String sql = "INSERT INTO ACCOUNTS.TRANSACTION (ID, SOURCE_ACCOUNT_NUMBER, DESTINATION_ACCOUNT_NUMBER, " +
                "AMOUNT, UPDATED_DATE) VALUES (ACCOUNTS.SEQ_TRANSACTION.NEXTVAL, :sourceAccountNumber, " +
                ":destinationAccountNumber, :amount, NOW())";

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("sourceAccountNumber", transaction.getSourceAccountNumber());
        params.addValue("destinationAccountNumber", transaction.getDestinationAccountNumber());
        params.addValue("amount", transaction.getAmount());

        int update = jdbcTemplate.update(sql, params);
        if (update != 1) {
            throw new FundTransferFailure(String.format("Could not insert transaction %s",
                    transaction));
        }

        LOG.info("transaction is saved");
    }
}
