package com.example.transferservice.dao.impl;

import com.example.transferservice.dao.AccountDao;
import com.example.transferservice.exception.FundTransferFailure;
import com.example.transferservice.model.Account;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import java.util.List;

/**
 * Concrete implementation for AccountDao interface
 */
public class AccountDaoImpl implements AccountDao {

    private static final Logger LOG = LoggerFactory.getLogger(AccountDaoImpl.class);

    @Autowired
    NamedParameterJdbcTemplate jdbcTemplate;

    @Override
    public void save(Account account) {
        LOG.info("save is invoked with: " + account);

        String sql = "UPDATE ACCOUNTS.ACCOUNT SET BALANCE=:balance WHERE ACCOUNT_NUMBER=:accountNumber";
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("balance", account.getBalance());
        params.addValue("accountNumber", account.getAccountNumber());
        int update = jdbcTemplate.update(sql, params);
        if (update != 1) {
            throw new FundTransferFailure(String.format("Could not update account %s with new balance",
                    account.getAccountNumber()));
        }

        LOG.info("account {} is updated", account.getAccountNumber());
    }

    @Override
    public List<Account> lockAccounts(List<Long> accountNumbers) {
        LOG.info("lockAccounts is invoked with: " + accountNumbers.toString());

        String sql = "SELECT * FROM ACCOUNTS.ACCOUNT WHERE ACCOUNT_NUMBER IN (:accountNumbers) FOR UPDATE";
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("accountNumbers", accountNumbers);
        List<Account> query = jdbcTemplate.query(sql, params, new AccountRowMapper());

        LOG.info("lockAccounts is returning accounts: " + query.toString());
        return query;
    }
}
