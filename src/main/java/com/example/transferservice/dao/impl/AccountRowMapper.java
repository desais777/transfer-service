package com.example.transferservice.dao.impl;

import com.example.transferservice.model.Account;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Maps account table columns of the resultset to Account object
 */
public class AccountRowMapper implements RowMapper<Account> {

    private static final Logger LOG = LoggerFactory.getLogger(AccountRowMapper.class);

    @Override
    public Account mapRow(ResultSet resultSet, int i) throws SQLException {
        return new Account.Builder()
                .withAccountNumber(resultSet.getLong("ACCOUNT_NUMBER"))
                .withBalance(resultSet.getBigDecimal("BALANCE"))
                .build();
    }
}