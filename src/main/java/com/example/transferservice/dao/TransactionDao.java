package com.example.transferservice.dao;

import com.example.transferservice.model.Transaction;

public interface TransactionDao {
    void save(Transaction transaction);
}
