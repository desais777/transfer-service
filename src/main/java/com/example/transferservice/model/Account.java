package com.example.transferservice.model;

import java.math.BigDecimal;

public class Account {

    Long accountNumber;
    BigDecimal balance;

    public Account() {
    }

    public Account(Builder builder) {
        this.accountNumber = builder.accountNumber;
        this.balance = builder.balance;
    }

    public Long getAccountNumber() {
        return accountNumber;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    @Override
    public String toString() {
        return "Account{" +
                "accountNumber=" + accountNumber +
                ", balance=" + balance +
                '}';
    }

    public static final class Builder {

        private Long accountNumber;
        private BigDecimal balance;

        public Builder() {
        }

        public Builder withAccountNumber(Long accountNumber) {
            this.accountNumber = accountNumber;
            return this;
        }

        public Builder withBalance(BigDecimal balance) {
            this.balance = balance;
            return this;
        }

        public Account build() {
            return new Account(this);
        }
    }
}
