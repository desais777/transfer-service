package com.example.transferservice.model;

import java.math.BigDecimal;

public class Transaction {

    Long transactionId;
    Long sourceAccountNumber;
    Long destinationAccountNumber;
    BigDecimal amount;

    public Transaction() {
    }

    public Transaction(Transaction.Builder builder) {
        this.transactionId = builder.transactionId;
        this.sourceAccountNumber = builder.sourceAccountNumber;
        this.destinationAccountNumber = builder.destinationAccountNumber;
        this.amount = builder.amount;
    }

    public Long getTransactionId() {
        return transactionId;
    }

    public Long getSourceAccountNumber() {
        return sourceAccountNumber;
    }

    public Long getDestinationAccountNumber() {
        return destinationAccountNumber;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public static final class Builder {

        Long transactionId;
        Long sourceAccountNumber;
        Long destinationAccountNumber;
        BigDecimal amount;

        public Builder() {
        }

        public Builder withTransactionId(Long transactionId) {
            this.transactionId = transactionId;
            return this;
        }

        public Builder withSourceAccountNumber(Long sourceAccountNumber) {
            this.sourceAccountNumber = sourceAccountNumber;
            return this;
        }

        public Builder withDestinationAccountNumber(Long destinationAccountNumber) {
            this.destinationAccountNumber = destinationAccountNumber;
            return this;
        }

        public Builder withAmount(BigDecimal amount) {
            this.amount = amount;
            return this;
        }

        public Transaction build() {
            return new Transaction(this);
        }
    }

}
