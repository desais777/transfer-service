package com.example.transferservice.config;

import com.example.transferservice.dao.AccountDao;
import com.example.transferservice.dao.TransactionDao;
import com.example.transferservice.dao.impl.AccountDaoImpl;
import com.example.transferservice.dao.impl.TransactionDaoImpl;
import com.example.transferservice.service.AccountTransferService;
import com.example.transferservice.web.FundTransferRequestValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;

import javax.sql.DataSource;

/**
 * Provides bean definitions for the application
 */
@Configuration
@ComponentScan({"com.example.accounttransfer"})
public class RootContext {

    private static final Logger LOG = LoggerFactory.getLogger(RootContext.class);

    @Bean
    public DataSource getDataSource() {
        return new EmbeddedDatabaseBuilder().setType(EmbeddedDatabaseType.H2)
                .addScript("scripts/schema-h2.sql").build();
    }

    @Bean
    public NamedParameterJdbcTemplate getNamedParameterJdbcTemplate(DataSource dataSource) {
        return new NamedParameterJdbcTemplate(dataSource);
    }

    @Bean
    public AccountTransferService getAccountTransferService() {
        return new AccountTransferService();
    }

    @Bean
    public AccountDao getAccountDao() {
        return new AccountDaoImpl();
    }

    @Bean
    public TransactionDao getTransactionDao() {
        return new TransactionDaoImpl();
    }

    @Bean
    public FundTransferRequestValidator getFundTransferRequestValidator() {
        return new FundTransferRequestValidator();
    }
}
