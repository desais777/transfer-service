package com.example.transferservice.web;

import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;

public class FundTransferRequest {

    Long sourceAccountNumber;
    Long destinationAccountNumber;
    BigDecimal amount;

    public FundTransferRequest() {
    }

    public FundTransferRequest(Builder builder) {
        sourceAccountNumber = builder.sourceAccountNumber;
        destinationAccountNumber = builder.destinationAccountNumber;
        amount = builder.amount;
    }

    @ApiModelProperty(value = "The source account number to transfer the funds from", example = "1", required = true)
    public Long getSourceAccountNumber() {
        return sourceAccountNumber;
    }

    @ApiModelProperty(value = "The destination account number to transfer the funds to", example = "2", required = true)
    public Long getDestinationAccountNumber() {
        return destinationAccountNumber;
    }

    @ApiModelProperty(value = "The amount to be transferred", example = "100.00", required = true)
    public BigDecimal getAmount() {
        return amount;
    }


    @Override
    public String toString() {
        return "FundTransferRequest{" +
                "sourceAccountNumber=" + sourceAccountNumber +
                ", destinationAccountNumber=" + destinationAccountNumber +
                ", amount=" + amount +
                '}';
    }

    public static final class Builder {

        Long sourceAccountNumber;
        Long destinationAccountNumber;
        BigDecimal amount;

        public Builder() {
        }

        public Builder withSourceAccountNumber(Long sourceAccountNumber) {
            this.sourceAccountNumber = sourceAccountNumber;
            return this;
        }

        public Builder withDestinationNumber(Long destinationAccountNumber) {
            this.destinationAccountNumber = destinationAccountNumber;
            return this;
        }

        public Builder withAmount(BigDecimal amount) {
            this.amount = amount;
            return this;
        }

        public FundTransferRequest build() {
            return new FundTransferRequest(this);
        }
    }

}
