package com.example.transferservice.web;

import io.swagger.annotations.ApiModelProperty;

public class ErrorResponse {

    String code;
    String message;
    String timestamp;
    String correlationId;

    public ErrorResponse() {
    }

    public ErrorResponse(Builder builder) {
        code = builder.code;
        message = builder.message;
        timestamp = builder.timestamp;
        correlationId = builder.correlationId;
    }

    @ApiModelProperty(value = "The HTTP response code related to this error", example = "404", required = true)
    public String getCode() {
        return code;
    }

    @ApiModelProperty(value = "The human readable error message to be able to understand the cause of the error", example = "Account not found: 10", required = true)
    public String getMessage() {
        return message;
    }

    @ApiModelProperty(value = "The date and time when the error is occurred", example = "2020-05-04T01:48:29.474", required = true)
    public String getTimestamp() {
        return timestamp;
    }

    @ApiModelProperty(value = "The correlationId, used for correlating log messages", example = "8250169100693459788", required = true)
    public String getCorrelationId() {
        return correlationId;
    }

    public static final class Builder {

        String code;
        String message;
        String timestamp;
        String correlationId;

        public Builder() {
        }

        public Builder withCode(String code) {
            this.code = code;
            return this;
        }

        public Builder withMessage(String message) {
            this.message = message;
            return this;
        }

        public Builder withTimestamp(String timestamp) {
            this.timestamp = timestamp;
            return this;
        }

        public Builder withCorrelationId(String correlationId) {
            this.correlationId = correlationId;
            return this;
        }

        public ErrorResponse build() {
            return new ErrorResponse(this);
        }
    }

}
