package com.example.transferservice.web;

import io.swagger.annotations.ApiModelProperty;

public class FundTransferResponse {

    String correlationId;
    String message;

    public FundTransferResponse() {
    }

    public FundTransferResponse(Builder builder) {
        correlationId = builder.correlationId;
        message = builder.message;
    }

    @ApiModelProperty(value = "The human readable message to describe the outcome", example = "Transfer is successful", required = true)
    public String getMessage() {
        return message;
    }

    @ApiModelProperty(value = "The correlationId, used for correlating log messages", example = "8250169100693459788", required = true)
    public String getCorrelationId() {

        return correlationId;
    }


    public static final class Builder {

        String correlationId;
        String message;

        public Builder() {
        }

        public Builder withCorrelationId(String correlationId) {
            this.correlationId = correlationId;
            return this;
        }

        public Builder withMessage(String message) {
            this.message = message;
            return this;
        }

        public FundTransferResponse build() {
            return new FundTransferResponse(this);
        }
    }
}
