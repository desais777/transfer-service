package com.example.transferservice.web;

import com.example.transferservice.exception.AccountNotFoundException;
import com.example.transferservice.exception.InsufficientFundsException;
import com.example.transferservice.exception.InvalidParameterException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.time.LocalDateTime;
import java.util.concurrent.ThreadLocalRandom;

@ControllerAdvice
public class TransferServiceExceptionHandler {

    private static final Logger LOG = LoggerFactory.getLogger(TransferServiceExceptionHandler.class);

    @ExceptionHandler({InvalidParameterException.class, InsufficientFundsException.class})
    public ResponseEntity<ErrorResponse> handleBadRequestException(Exception e) {
        LOG.error("Exception: " + e.getMessage(), e);
        ErrorResponse response = new ErrorResponse.Builder()
                .withCode(HttpStatus.BAD_REQUEST.toString())
                .withCorrelationId(Long.toString(ThreadLocalRandom.current().nextLong(Long.MAX_VALUE)))
                .withMessage(e.getMessage())
                .withTimestamp(LocalDateTime.now().toString())
                .build();
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
    }

    @ExceptionHandler(AccountNotFoundException.class)
    public ResponseEntity<ErrorResponse> handleAccountNotFoundException(Exception e) {
        LOG.error("Exception: " + e.getMessage(), e);
        ErrorResponse response = new ErrorResponse.Builder()
                .withCode(HttpStatus.NOT_FOUND.toString())
                .withCorrelationId(Long.toString(ThreadLocalRandom.current().nextLong(Long.MAX_VALUE)))
                .withMessage(e.getMessage())
                .withTimestamp(LocalDateTime.now().toString())
                .build();
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
    }
}
