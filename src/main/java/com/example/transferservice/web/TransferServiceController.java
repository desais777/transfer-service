package com.example.transferservice.web;

import com.example.transferservice.model.Transaction;
import com.example.transferservice.service.AccountTransferService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.util.Json;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.concurrent.ThreadLocalRandom;

@Api
@CrossOrigin
@Controller
@Validated
@RequestMapping(value = "/v1/transferservice", produces = MediaType.APPLICATION_JSON_VALUE)
public class TransferServiceController {

    private static final Logger LOG = LoggerFactory.getLogger(TransferServiceController.class);

    @Autowired
    private AccountTransferService service;

    @Autowired
    private FundTransferRequestValidator requestValidator;

    @RequestMapping(value = {"/fundtransfer"}, method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    @ApiOperation(value = "Transfer funds from source account number to destination account number.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, response = FundTransferResponse.class, message = "Success"),
            @ApiResponse(code = 400, response = ErrorResponse.class, message = "Invalid input parameters"),
            @ApiResponse(code = 404, response = ErrorResponse.class, message = "The account number is not found")
    })

    public ResponseEntity<String> transferFunds(@RequestBody final FundTransferRequest fundTransferRequest) {
        LOG.info("service: " + service);

        requestValidator.validate(fundTransferRequest);
        Transaction transaction = new Transaction.Builder()
                .withSourceAccountNumber(fundTransferRequest.getSourceAccountNumber())
                .withDestinationAccountNumber(fundTransferRequest.getDestinationAccountNumber())
                .withAmount(fundTransferRequest.getAmount())
                .build();
        service.transfer(transaction);

        FundTransferResponse response = new FundTransferResponse.Builder()
                .withCorrelationId(Long.toString(ThreadLocalRandom.current().nextLong(Long.MAX_VALUE)))
                .withMessage("Transfer is successful")
                .build();
        return ResponseEntity.status(HttpStatus.OK).body(Json.pretty(response));
    }

}
