package com.example.transferservice.web;

import com.example.transferservice.exception.InvalidParameterException;

public class FundTransferRequestValidator {

    public void validate(FundTransferRequest request) {
        if (request.getSourceAccountNumber() == null) {
            throw new InvalidParameterException("Source account number must be supplied.");
        }
        if (request.getDestinationAccountNumber() == null) {
            throw new InvalidParameterException("Destination account number must be supplied.");
        }
        if (request.getSourceAccountNumber().equals(request.getDestinationAccountNumber())) {
            throw new InvalidParameterException("Source and Destination account numbers cannot be same.");
        }
        if (request.getAmount() == null) {
            throw new InvalidParameterException("Amount must be supplied.");
        }
    }
}
