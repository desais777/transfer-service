Overview
--------
It's a TransferService API that provides endpoint to transfer funds from one account to another.


Clone to local machine
--------
```
git clone https://bitbucket.org/desais777/transfer-service.git
```

How to run
----------
Import it to your favourite IDE. Run following class as java application to start the service:
```
com.example.transferservice.TransferServiceApplication
```

Data setup
----------
H2 Database schema is located at:
```
src/main/resources/scripts/schema-h2.sql
```

The script creates the tables (Account, Transaction) and adds two accounts to the database.
```
Account number: 1 with balance: 10000
Account number: 2 with balance: 20000
```

How to invoke the endpoint:
---------------------------
Once the TransferServiceApplication is run, the application runs on port number 8080.

API endpoint:
```
http://localhost:8080/v1/transferservice/fundtransfer
```
The swagger documentation is available at:
```
/transfer-service/generated-doc/document.html
/transfer-service/generated-doc/swagger.json
```

How to verify the endpoint execution
------------------------------------
The console log in the IDE can be used to verify the endpoint execution.

POST request with body:
```
{
	"sourceAccountNumber": "1",
	"destinationAccountNumber": "2",
	"amount":"1000.00"
}
```

Response:
```
{
  "correlationId": "2162054427396790385",
  "message": "Transfer is successful"
}
```

Error request:
```
{
	"sourceAccountNumber": "10",
	"destinationAccountNumber": "2",
	"amount":"1000.00"

}
```

Error Response:
```
{
    "code": "400 BAD_REQUEST",
    "message": "Account not found: 10",
    "timestamp": "2020-05-04T01:48:29.474",
    "correlationId": "8250169100693459788"
}
```
